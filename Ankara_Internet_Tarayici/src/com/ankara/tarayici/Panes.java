package com.ankara.tarayici;

import com.ankara.tarayici.ui.ToggleSwitch;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.geometry.VPos;
import javafx.scene.control.*;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;

/**
 * Created by Berk Can on 5.08.2017.
 */
public class Panes {

    public static ToggleGroup groupViewPage = new ToggleGroup();
    public static ComboBox comboBox,comboBox1,comboBox2;
    public static ToggleSwitch toggleSwitch;
    public static RadioButton rb1,rb2,rb3;
    public static Label userNameUserPage,pwUserPage;
    public static TextField userTextFieldUserPage,pwBoxUserPage;
    public static Button btnUserPage;
    public static Text viewTextViewPage,homePageTitleViewPage,initiallyTextViewPage,newTabOpenTextViewPage
            ,ContinueWhereleftTextViewPage,openSpecificPageTextViewPage;

    public static GridPane usersPane() {
        GridPane grid = new GridPane();
        grid.setHgap(10);
        grid.setVgap(10);

        Text scenetitle = new Text("Ankara İnternet Tarayıcısı'na Hoşgeldiniz");
        scenetitle.setFont(Font.font("Tahoma", FontWeight.NORMAL, 20));
        grid.add(scenetitle, 0, 0, 2, 1);

        userNameUserPage = new Label("EMail Adresi:");
        grid.add(userNameUserPage, 0, 1);

        userTextFieldUserPage = new TextField();
        grid.add(userTextFieldUserPage, 1, 1);

        pwUserPage = new Label("Şifre:");
        grid.add(pwUserPage, 0, 2);

        pwBoxUserPage = new PasswordField();
        grid.add(pwBoxUserPage, 1, 2);

        btnUserPage = new Button("Giriş Yap");
        HBox hbBtn = new HBox(10);
        hbBtn.setAlignment(Pos.BOTTOM_RIGHT);
        hbBtn.getChildren().add(btnUserPage);
        grid.add(hbBtn, 1, 4);

        final Text actiontarget = new Text();
        grid.add(actiontarget, 1, 6);

        btnUserPage.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
                actiontarget.setFill(Color.FIREBRICK);
                actiontarget.setText("Giriş Yap a tıklandı");
            }
        });

        return grid;
    }

    public static GridPane viewPane() {
        GridPane grid = new GridPane();
        grid.setHgap(10);
        grid.setVgap(10);
        grid.setPadding(new Insets(10, 10, 10, 10));

        viewTextViewPage = new Text("Görünüm");
        GridPane.setValignment(viewTextViewPage, VPos.CENTER);
        grid.add(viewTextViewPage, 2, 0);

        homePageTitleViewPage = new Text("Ana Sayfa Düğmesini Göster");
        grid.add(homePageTitleViewPage, 0, 1);

        toggleSwitch = new ToggleSwitch();
        GridPane.setValignment(toggleSwitch, VPos.CENTER);
        grid.add(toggleSwitch,1,1);

        Text fontSize = new Text("Yazı Tipi Boyutu");
        grid.add(fontSize, 0, 2, 2, 1);

        ObservableList<String> fontSizeList =
                FXCollections.observableArrayList(
                        "Çok Küçük",
                        "Küçük",
                        "Orta(Önerilir)",
                        "Büyük",
                        "Çok Büyük"
                );
        comboBox = new ComboBox(fontSizeList);
        comboBox.setValue("Orta(Önerilir)");
        GridPane.setValignment(toggleSwitch, VPos.CENTER);
        grid.add(comboBox,1,2);

        Text customizeFont = new Text("Yazı Tipini Özelleştir");
        grid.add(customizeFont, 0, 3, 2, 1);

        ObservableList<String> customizeFontList =
                FXCollections.observableArrayList(
                        "Times New Roman",
                        "Courier New",
                        "Amiga Forever Pro",
                        "Calibri",
                        "Calibri(Gövde)"
                );
        comboBox1 = new ComboBox(customizeFontList);
        comboBox1.setValue("Times New Roman");
        grid.add(comboBox1,1,3);

        Text pageZoom = new Text("Sayfa Yakınlaştırma");
        grid.add(pageZoom, 0, 4, 2, 1);

        ObservableList<String> pageZoomList =
                FXCollections.observableArrayList(
                        "25%",
                        "33%",
                        "50%",
                        "67%",
                        "75%",
                        "80%",
                        "90%",
                        "100%",
                        "110%",
                        "125%",
                        "150%",
                        "175%",
                        "200%",
                        "250%",
                        "300%",
                        "400%",
                        "500%"
                );
        comboBox2 = new ComboBox(pageZoomList);
        comboBox2.setValue("100%");
        grid.add(comboBox2,1,4);

        initiallyTextViewPage = new Text("Başlangıçta");
        GridPane.setValignment(viewTextViewPage, VPos.CENTER);
        grid.add(initiallyTextViewPage, 2, 5);

        newTabOpenTextViewPage = new Text("Yeni Sekme Sayfasını Aç");
        grid.add(newTabOpenTextViewPage, 0, 6, 2, 1);

        rb1 = new RadioButton("");
        rb1.setSelected(false);
        rb1.setToggleGroup(groupViewPage);
        grid.add(rb1, 1, 6, 2, 1);

        ContinueWhereleftTextViewPage= new Text("Kaldığım Yerden Devam Et");
        grid.add(ContinueWhereleftTextViewPage, 0, 7, 2, 1);

        rb2 = new RadioButton("");
        rb2.setSelected(false);
        rb2.setToggleGroup(groupViewPage);

        grid.add(rb2, 1, 7, 2, 1);

        openSpecificPageTextViewPage = new Text("Belirli Bir Sayfayı Aç");
        grid.add(openSpecificPageTextViewPage, 0, 8, 2, 1);

        rb3 = new RadioButton("");
        rb3.setSelected(true);
        rb3.setToggleGroup(groupViewPage);
        grid.add(rb3, 1, 8, 2, 1);

        groupViewPage.selectedToggleProperty().addListener(new ChangeListener<Toggle>(){
            public void changed(ObservableValue<? extends Toggle> ov,
                                Toggle old_toggle, Toggle new_toggle) {
                if (groupViewPage.getSelectedToggle() != null) {

                }
            }
        });

        return grid;
    }

    public static GridPane privacySecurityPane() {
        GridPane grid = new GridPane();
        grid.setHgap(10);
        grid.setVgap(10);

        Text category = new Text("privacySecurityPane:");
        grid.add(category, 1, 0);

        Text chartTitle = new Text("Current Year");
        grid.add(chartTitle, 2, 0);

        Text chartSubtitle = new Text("Goods and Services");
        grid.add(chartSubtitle, 1, 1, 2, 1);

        Text goodsPercent = new Text("Goods\n80%");
        GridPane.setValignment(goodsPercent, VPos.BOTTOM);
        grid.add(goodsPercent, 0, 2);


        Text servicesPercent = new Text("Services\n20%");
        GridPane.setValignment(servicesPercent, VPos.TOP);
        grid.add(servicesPercent, 3, 2);

        return grid;
    }

    public static GridPane PasswordsandFormsPane() {
        GridPane grid = new GridPane();
        grid.setHgap(10);
        grid.setVgap(10);

        Text category = new Text("PasswordsandFormsPane:");
        grid.add(category, 1, 0);

        Text chartTitle = new Text("Current Year");
        grid.add(chartTitle, 2, 0);

        Text chartSubtitle = new Text("Goods and Services");
        grid.add(chartSubtitle, 1, 1, 2, 1);

        Text goodsPercent = new Text("Goods\n80%");
        GridPane.setValignment(goodsPercent, VPos.BOTTOM);
        grid.add(goodsPercent, 0, 2);

        Text servicesPercent = new Text("Services\n20%");
        GridPane.setValignment(servicesPercent, VPos.TOP);
        grid.add(servicesPercent, 3, 2);

        return grid;
    }

    public static GridPane ProxyFormsPane() {
        GridPane grid = new GridPane();
        grid.setHgap(10);
        grid.setVgap(10);

        Text category = new Text("PasswordsandFormsPane:");
        grid.add(category, 1, 0);

        Text chartTitle = new Text("Current Year");
        grid.add(chartTitle, 2, 0);

        Text chartSubtitle = new Text("Goods and Services");
        grid.add(chartSubtitle, 1, 1, 2, 1);

        Text goodsPercent = new Text("Goods\n80%");
        GridPane.setValignment(goodsPercent, VPos.BOTTOM);
        grid.add(goodsPercent, 0, 2);

        Text servicesPercent = new Text("Services\n20%");
        GridPane.setValignment(servicesPercent, VPos.TOP);
        grid.add(servicesPercent, 3, 2);

        return grid;
    }

    public static GridPane LanguagesPane() {
        GridPane grid = new GridPane();
        grid.setHgap(10);
        grid.setVgap(10);

        Text category = new Text("LanguagesPane :");
        grid.add(category, 1, 0);

        Text chartTitle = new Text("Current Year");
        grid.add(chartTitle, 2, 0);

        Text chartSubtitle = new Text("Goods and Services");
        grid.add(chartSubtitle, 1, 1, 2, 1);

        Text goodsPercent = new Text("Goods\n80%");
        GridPane.setValignment(goodsPercent, VPos.BOTTOM);
        grid.add(goodsPercent, 0, 2);

        Text servicesPercent = new Text("Services\n20%");
        GridPane.setValignment(servicesPercent, VPos.TOP);
        grid.add(servicesPercent, 3, 2);

        return grid;
    }

    public static GridPane DownloadsPane() {
        GridPane grid = new GridPane();
        grid.setHgap(10);
        grid.setVgap(10);

        Text category = new Text("DownloadsPane:");
        grid.add(category, 1, 0);

        Text chartTitle = new Text("Current Year");
        grid.add(chartTitle, 2, 0);

        Text chartSubtitle = new Text("Goods and Services");
        grid.add(chartSubtitle, 1, 1, 2, 1);

        Text goodsPercent = new Text("Goods\n80%");
        GridPane.setValignment(goodsPercent, VPos.BOTTOM);
        grid.add(goodsPercent, 0, 2);

        Text servicesPercent = new Text("Services\n20%");
        GridPane.setValignment(servicesPercent, VPos.TOP);
        grid.add(servicesPercent, 3, 2);

        return grid;
    }

    public static GridPane ResetBrowserPane() {
        GridPane grid = new GridPane();
        grid.setHgap(10);
        grid.setVgap(10);

        Text category = new Text("ResetBrowserPane:");
        grid.add(category, 1, 0);

        Text chartTitle = new Text("Current Year");
        grid.add(chartTitle, 2, 0);

        Text chartSubtitle = new Text("Goods and Services");
        grid.add(chartSubtitle, 1, 1, 2, 1);

        Text goodsPercent = new Text("Goods\n80%");
        GridPane.setValignment(goodsPercent, VPos.BOTTOM);
        grid.add(goodsPercent, 0, 2);

        Text servicesPercent = new Text("Services\n20%");
        GridPane.setValignment(servicesPercent, VPos.TOP);
        grid.add(servicesPercent, 3, 2);

        return grid;
    }

    public static GridPane licenseInformationPane() {
        GridPane grid = new GridPane();
        grid.setHgap(10);
        grid.setVgap(10);
        Text category = new Text("Ankara İnternet Tarayıcı'sı Lisan Bilgileri");
        grid.add(category, 1, 1);
        Text chartSubtitle = new Text("Ankara İnternet Tarayıcı aşağıda belirtilen açık kaynak ürünleri kullanılarak geliştirilmiş ve geliştirilmeye devam etmektedir.");
        grid.add(chartSubtitle, 1, 4);
        Text licenseOne = new Text("1.  javafx-web-browser  adlı GitHub reposu");
        GridPane.setValignment(licenseOne, VPos.BOTTOM);
        grid.add(licenseOne, 1, 7);
        Text licenseOneExplanation = new Text("javafx-web-browser  adlı GitHub reposundan alınan kodlar " +
                "ankAra Tarayıcı projesinin çekirdeğini oluşturmaktadır.Alınan\nkodlar üzerinde değişikler yapılmış," +
                "bazı kısımlar çıkarılmış ve yeni bölümler eklenmiştir. Reponun herhangi bir açık kaynak\n" +
                "lisansı bulunmadığı için repo sahibi olan Steve Byungseon Kim ile iletişime geçilerek," +
                "repo kullanım izni istenmiştir ve onay\nalınmıştır.");
        GridPane.setValignment(licenseOneExplanation, VPos.BOTTOM);
        grid.add(licenseOneExplanation, 1, 8);
        Text licenseTwo = new Text("2.  freepik.com adlı site üzerinden projenin iconları temin edilmiştir ");
        GridPane.setValignment(licenseTwo, VPos.BOTTOM);
        grid.add(licenseTwo, 1, 10);
        Text licenseWtoExplanation = new Text("Ankara İnternet Tarayıcı'sı adlı proje içerisinde freepik.com adlı site üzerinden," +
                "proje içerisinde kullanılan iconlar alınmıştır.Kullanılan\niconların hepsi ücretsiz olup,açık kaynak " +
                "dünyasına sunulan iconlardır.");
        GridPane.setValignment(licenseWtoExplanation, VPos.BOTTOM);
        grid.add(licenseWtoExplanation, 1, 11);
        return grid;
    }

}
