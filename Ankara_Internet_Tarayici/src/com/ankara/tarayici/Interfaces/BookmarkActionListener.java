package com.ankara.tarayici.Interfaces;

import com.ankara.tarayici.Events.BookmarkActionEvent;

import java.util.EventListener;
/**
 * Developer Berk Can
 */

public interface BookmarkActionListener extends EventListener {
	public void onBookmarkButtonClick(BookmarkActionEvent actionEvent);
	public boolean isBookmarkThere(BookmarkActionEvent actionEvent);
}
