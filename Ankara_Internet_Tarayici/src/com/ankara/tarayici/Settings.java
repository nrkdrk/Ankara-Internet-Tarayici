package com.ankara.tarayici;

import com.ankara.tarayici.tools.TabOperations;
import javafx.animation.FadeTransition;
import javafx.animation.ParallelTransition;
import javafx.animation.SequentialTransition;
import javafx.animation.TranslateTransition;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.beans.binding.Bindings;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.*;
import javafx.scene.control.*;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.image.*;
import javafx.scene.input.*;
import javafx.scene.layout.*;
import javafx.scene.web.WebEngine;
import javafx.scene.web.WebHistory;
import javafx.scene.web.WebView;
import javafx.util.Duration;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;


/**
 * Created by Berk Can on 5.08.2017.
 */
public class Settings  extends Region {

    private final double HISTORYLIST_WIDTH = 200.0;
    private boolean isHistoryListOpened;
    protected WebEngine engine;
    private String defaultURL;
    private String downloadPath;
    private WebView browserView;
    private WebHistory history;
    private TextField urlText;
    private ListView<WebHistory.Entry> historyList;
    private Button backButton,refButton, bookmarkButton, nextButton;
    private MenuButton menuButton;
    private BorderPane root;
    private List<com.ankara.tarayici.Interfaces.BookmarkActionListener> bookmarkListeners;
    javafx.scene.image.Image image;

    public Settings(String defaultURL, String downloadPath) {
        this.defaultURL = defaultURL;
        this.downloadPath = downloadPath;
        image = new javafx.scene.image.Image(getClass().getResource("images/ankaratarayici.png").toExternalForm());

        browserView = new WebView();
        engine = browserView.getEngine();
        history = engine.getHistory();
        bookmarkListeners = new ArrayList<com.ankara.tarayici.Interfaces.BookmarkActionListener>();

        createScene();
    }

    private void createScene() {
        Settings.MouseHandler myHandler = new Settings.MouseHandler();

        final Tooltip backButtonTooltip = new Tooltip();
        backButtonTooltip.setText("Geri");

        backButton = new Button("Geri");
        backButton.disableProperty().bind(history.currentIndexProperty().lessThanOrEqualTo(0)); // Disable Condition
        backButton.setOnMouseClicked(myHandler);
        backButton.setTooltip(backButtonTooltip);

        refButton = new Button("Yenile");
        refButton.setTooltip(backButtonTooltip);
        refButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override public void handle(ActionEvent e) {
                engine.reload();
            }
        });

        BufferedImage image = new BufferedImage(25, 25, BufferedImage.TYPE_INT_RGB);
        Graphics graphics = image.getGraphics();
        graphics.setColor(Color.WHITE);
        graphics.fillRect(0, 0, 25, 25);
        graphics.setColor(Color.RED);
        graphics.fillRect(2, 11, 21, 3);
        graphics.fillRect(11, 2, 3, 21);

        urlText = new TextField();
        urlText.setOnMouseClicked(myHandler);

        urlText.setId("textField");
        StackPane UrlLayout = new StackPane();
        UrlLayout.getChildren().addAll(urlText);
        UrlLayout.getStylesheets().add(this.getClass().getResource("textfield.css").toExternalForm());
        urlTextEvents();
        urlText.setText("ankara.internet.tarayici//ayarlar");

        final Tooltip bookmarkButtonTooltip = new Tooltip();
        bookmarkButtonTooltip.setText("Sık Kullanılanlara Ekle");
        bookmarkButton = new Button("Sık Kullanılanlar");
        bookmarkButton.setDisable(true);
        bookmarkButton.setTooltip(bookmarkButtonTooltip);
        bookmarkButton.setOnMouseClicked(myHandler);
        bookmarkButton.setVisible(false);

        final Tooltip nextButtonTooltip = new Tooltip();
        nextButtonTooltip.setText("İleri");
        nextButton = new Button("İleri");
        nextButton.disableProperty().bind(
                history.currentIndexProperty().greaterThanOrEqualTo(Bindings.size(history.getEntries()).subtract(1)));
        nextButton.setOnMouseClicked(myHandler);
        nextButton.setTooltip(nextButtonTooltip);


        menuButton = new MenuButton("Menu", null);
        javafx.scene.control.Menu menu=new javafx.scene.control.Menu("Yardım");

        javafx.scene.control.MenuItem menuItem9 = new javafx.scene.control.MenuItem("Çıkış");
        javafx.scene.control.MenuItem menuItem8 = new javafx.scene.control.MenuItem("Ayarlar");
        javafx.scene.control.MenuItem menuItem7 = new javafx.scene.control.MenuItem("Diğer Araçlar");
        javafx.scene.control.MenuItem menuItem6 = new javafx.scene.control.MenuItem("Yer İşaretleri");
        javafx.scene.control.MenuItem menuItem5 = new javafx.scene.control.MenuItem("İndirilenler");
        javafx.scene.control.MenuItem menuItem3 = new javafx.scene.control.MenuItem("Yeni Gizli Pencere");
        javafx.scene.control.MenuItem menuItem2 = new javafx.scene.control.MenuItem("Yeni Pencere");
        javafx.scene.control.MenuItem menuItem1 = new javafx.scene.control.MenuItem("Yeni Sekme");

        javafx.scene.control.MenuItem menuItem1_1 = new javafx.scene.control.MenuItem("Ankara İnternet Tarayıcı'sı Hakkında");
        javafx.scene.control.MenuItem menuItem1_2 = new javafx.scene.control.MenuItem("Kısa Yollar");
        javafx.scene.control.MenuItem menuItem1_3 = new javafx.scene.control.MenuItem("Yardım Merkezi");
        javafx.scene.control.MenuItem menuItem1_4 = new javafx.scene.control.MenuItem("Sorun Bildir");

        menu.getItems().add(0,menuItem1_4);
        menu.getItems().add(0,menuItem1_3);
        menu.getItems().add(0,menuItem1_2);
        menu.getItems().add(0,menuItem1_1);

        menuItem1.setAccelerator(new KeyCodeCombination(KeyCode.T, KeyCombination.CONTROL_DOWN));
        menuItem2.setAccelerator(new KeyCodeCombination(KeyCode.N, KeyCombination.CONTROL_DOWN));
        menuItem9.setAccelerator(new KeyCodeCombination(KeyCode.Q, KeyCombination.CONTROL_DOWN));
        menuItem1_1.setAccelerator(new KeyCodeCombination(KeyCode.A, KeyCombination.CONTROL_DOWN));
        menuItem1_2.setAccelerator(new KeyCodeCombination(KeyCode.R, KeyCombination.CONTROL_DOWN));

        menuButton.getItems().add(0,menuItem9);
        menuButton.getItems().add(0,menu);
        menuButton.getItems().add(0,menuItem8);
        menuButton.getItems().add(0,menuItem7);
        menuButton.getItems().add(0,menuItem6);
        menuButton.getItems().add(0,menuItem5);
        menuButton.getItems().add(0,menuItem3);
        menuButton.getItems().add(0,menuItem2);
        menuButton.getItems().add(0,menuItem1);
        //menuButton.getItems().add(0,);

        menuItem1.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                System.out.println("Option 1 selected and new tab");
                TabOperations.createNewTab(Main.tabPane,defaultURL,downloadPath);
            }
        });

        menuItem2.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                Application.launch(Main.class);
                System.out.println("Option 2 selected");
            }
        });

        menuItem3.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                System.out.println("Option 3 selected");
            }
        });

        menuItem5.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                System.out.println("Option 5 selected");
            }
        });

        menuItem6.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                System.out.println("Option 6 selected");
            }
        });

        menuItem7.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                System.out.println("Option 7 selected");
            }
        });

        menuItem8.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                TabOperations.createNewTabSettings(Main.tabPane,defaultURL,downloadPath);
            }
        });

        menuItem9.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                Platform.exit();
                System.out.println("Option 9 selected");
            }
        });

        menuItem1_1.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                com.ankara.tarayici.PaneAndPages.AlertDialogs.about(Main.image);
                System.out.println("Option 9 selected");
            }
        });

        menuItem1_2.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                com.ankara.tarayici.PaneAndPages.AlertDialogs.shortcuts(Main.image);
                System.out.println("Option 9 selected");
            }
        });

        menuItem1_3.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                System.out.println("Option 9 selected");
            }
        });

        menuItem1_4.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                System.out.println("Option 9 selected");
            }
        });

        HBox browserBar;
        if (refButton.isVisible()==false){
            browserBar  = new HBox(backButton,nextButton,urlText,menuButton);
        }else {
            browserBar = new HBox(backButton,nextButton,refButton,urlText,menuButton);
        }
        HBox.setHgrow(urlText, Priority.ALWAYS);

        historyList = new ListView<>();
        historyList.setItems(history.getEntries());
        historyList.setMaxWidth(0.0);
        isHistoryListOpened = false;

        browserView.setOnKeyPressed(new EventHandler<KeyEvent>() {
            @Override
            public void handle(KeyEvent ke) {
                if (ke.isControlDown()) {
                    if (ke.getCode() == KeyCode.LEFT && !backButton.isDisable()) { // history back
                        goBack();
                    } else if (ke.getCode() == KeyCode.RIGHT && !nextButton.isDisable()) { // history forward
                        goForward();
                    }
                }
            }
        });

        root = new BorderPane();
        root.setPadding(new javafx.geometry.Insets(3, 2, 2, 2));
        root.setTop(browserBar);
        root.setCenter(browserView);
        root.setRight(historyList);
        root.setLeft(addVBox());

        getChildren().add(root);
    }

    @Override
    protected void layoutChildren() {
        layoutInArea(root, 0.0, 0.0, getWidth(), getHeight(), 0.0, HPos.CENTER, VPos.CENTER);
    }

    public void goBack() { history.go(-1); }

    public void goForward() { history.go(1); }

    public void goURL(String URL) { engine.load(URL); }

    public void setDefaultURL(String URL) { defaultURL = URL; }

    public void setDownloadPath(String Path) { downloadPath = Path; }

    public void executeJavaScript(String command) {
        engine.executeScript(command);
    }

    private class MouseHandler implements EventHandler<MouseEvent> {
        @Override
        public void handle(MouseEvent e) {
            if (e.getSource().equals(backButton)) {
                if (e.getClickCount() == 1)	goBack();
            } else if (e.getSource().equals(urlText)) {
                if (e.getClickCount() == 2)
                    urlText.setText("");
                else if (e.getClickCount() == 1)
                    urlText.selectAll();
            } else if (e.getSource().equals(bookmarkButton)) {
                if (e.getClickCount() == 1) {
                    addBookmarks();
                }
            } else if (e.getSource().equals(nextButton)) {
                if (e.getClickCount() == 1)	goForward();
            }
            e.consume();
        }
    }

    public void animationListDoor() {
        if (!isHistoryListOpened) {
            historyList.setMaxWidth(HISTORYLIST_WIDTH);

            FadeTransition ft = new FadeTransition(Duration.millis(500.0));
            ft.setByValue(0.3);
            ft.setCycleCount(2);
            ft.setAutoReverse(true);

            TranslateTransition tt = new TranslateTransition(Duration.millis(10.0));
            tt.setFromX(0.0);
            tt.setToX(20.0);
            tt.setCycleCount(50);
            tt.setAutoReverse(true);

            ParallelTransition pt = new ParallelTransition(historyList, ft,  tt);
            pt.play();
            isHistoryListOpened = true;
        } else {
            TranslateTransition t1 = new TranslateTransition(Duration.millis(10.0));
            t1.setByX(0.0);
            t1.setCycleCount(1);
            t1.setAutoReverse(true);

            TranslateTransition t2 = new TranslateTransition(Duration.millis(10.0));
            t2.setFromX(0.0);
            t2.setToX(20.0);
            t2.setCycleCount(50);
            t2.setAutoReverse(true);

            TranslateTransition t3 = new TranslateTransition(Duration.millis(500.0));
            t3.setByX(HISTORYLIST_WIDTH);
            t3.setCycleCount(1);
            t3.setAutoReverse(true);

            SequentialTransition st = new SequentialTransition (historyList, t1, t2, t3);
            st.setOnFinished(event -> {
                historyList.setMaxWidth(0.0);
                isHistoryListOpened = false;
            });
            st.play();
        }
    }

    public boolean getIsHistoryListOpened() { return isHistoryListOpened; }

    private void urlTextEvents() {
        urlText.setOnKeyPressed(e -> {
            if (e.getCode() == KeyCode.ENTER) {
                String url;
                if (isURL(urlText.getText()))
                    url = urlText.getText();
                else
                    url = "https://www.ankara.ilbilge.com.tr/ara.php?url=" +
                            urlText.getText() + "&ie=UTF-8";
                engine.load(url);
                e.consume();
            }
        });
    }

    private boolean isURL(String url) {
        int idx = url.indexOf("://");
        if (idx == -1) return false;
        else return true;
    }

    public ImageView loadFavicon(String location) {
        ImageView favicon = null;
        try {
            String faviconUrl = String.format("http://www.google.com/s2/favicons?domain_url=%s", URLEncoder.encode(location, "UTF-8"));
            favicon = new ImageView(new javafx.scene.image.Image(faviconUrl, true));
        } catch (UnsupportedEncodingException ex) {
        }
        return favicon;
    }


    private synchronized void addBookmarks() {
        fireBookmarkEvent(0);
        bookmarkButton.setDisable(true);
    }

    private synchronized void fireBookmarkEvent(int whichEvent) {
        String title = engine.getTitle();
        String location = engine.getLocation();
        if (title == null || title == "") title = location;
        ImageView icon = loadFavicon(engine.getDocument().getBaseURI());
       com.ankara.tarayici.Events.BookmarkActionEvent event = new com.ankara.tarayici.Events.BookmarkActionEvent(this, title, location, icon);
        Iterator<com.ankara.tarayici.Interfaces.BookmarkActionListener> listeners = bookmarkListeners.iterator();
        while( listeners.hasNext() ) {
            if (whichEvent == 0)
                (listeners.next()).onBookmarkButtonClick(event);
            else bookmarkButton.setDisable((listeners.next()).isBookmarkThere(event));
        }
    }

    public VBox addVBox(){
        VBox vbox = new VBox();
        vbox.setPadding(new javafx.geometry.Insets(10));
        vbox.setSpacing(8);
        vbox.setStyle("-fx-background-color: #6B6B6B;");


        Hyperlink users=new Hyperlink();
        users.setText("Kullanıcı");
        users.setStyle("-fx-text-fill: #ffff;");
        Hyperlink View=new Hyperlink();
        View.setText("Görünüm");
        View.setStyle("-fx-text-fill: #ffff;");
        Hyperlink privacySecurity =new Hyperlink();
        privacySecurity.setText("Gizlilik Güvenlik");
        privacySecurity.setStyle("-fx-text-fill: #ffff;");
        Hyperlink PasswordsandForms=new Hyperlink();
        PasswordsandForms.setText("Şifreler ve Formlar");
        PasswordsandForms.setStyle("-fx-text-fill: #ffff;");
        Hyperlink ProxyForms=new Hyperlink();
        ProxyForms.setText("Proxy Seçenekleri");
        ProxyForms.setStyle("-fx-text-fill: #ffff;");
        Hyperlink Languages=new Hyperlink();
        Languages.setText("Diller");
        Languages.setStyle("-fx-text-fill: #ffff;");
        Hyperlink Downloads=new Hyperlink();
        Downloads.setText("İndirilenler");
        Downloads.setStyle("-fx-text-fill: #ffff;");
        Hyperlink ResetBrowser=new Hyperlink();
        ResetBrowser.setText("Tarayıcıyı Sıfırla");
        ResetBrowser.setStyle("-fx-text-fill: #ffff;");

        Hyperlink licenseInformation=new Hyperlink();
        licenseInformation.setText("Lisans Bilgileri");
        licenseInformation.setStyle("-fx-text-fill: #ffff;");
        vbox.getChildren().addAll(
                users,
                View,
                privacySecurity,
                PasswordsandForms,
                ProxyForms,
                Languages,
                Downloads,
                ResetBrowser,
                licenseInformation);

        users.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                System.out.print(users);
                root.setCenter(Panes.usersPane());
            }
        });

        View.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                System.out.print(View);
                root.setCenter(Panes.viewPane());
            }
        });

        privacySecurity.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                System.out.print(privacySecurity);
                root.setCenter(Panes.privacySecurityPane());
            }
        });

        PasswordsandForms.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                System.out.print(PasswordsandForms);
                root.setCenter(Panes.PasswordsandFormsPane());
            }
        });

        ProxyForms.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                System.out.print(PasswordsandForms);
                root.setCenter(Panes.ProxyFormsPane());
            }
        });

        Languages.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                System.out.print(Languages);
                root.setCenter(Panes.LanguagesPane());
            }
        });

        Downloads.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                System.out.print(Downloads);
                root.setCenter(Panes.DownloadsPane());
            }
        });

        ResetBrowser.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                System.out.print(ResetBrowser);
                root.setCenter(Panes.ResetBrowserPane());
            }
        });

        licenseInformation.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                System.out.print(licenseInformation);
                root.setCenter(Panes.licenseInformationPane());
            }
        });

        return vbox;
    }

}