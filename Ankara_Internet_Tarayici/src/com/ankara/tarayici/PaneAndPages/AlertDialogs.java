package com.ankara.tarayici.PaneAndPages;

import com.ankara.tarayici.Browser;
import javafx.application.Platform;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.layout.GridPane;
import javafx.stage.DirectoryChooser;
import javafx.stage.Stage;

import java.io.File;
import java.util.Optional;


/**
 * Created by Berk Can on 16.09.2018.
 */
public class AlertDialogs {

    public static String downloadFilePath;
    public static String defaultURL;

    public static void setHomepageURL(String URL,javafx.scene.image.Image image,TabPane tabPane) {
        defaultURL=URL;
        TextInputDialog dialog = new TextInputDialog(defaultURL);
        dialog.setTitle("Ana Sayfa Adresi");
        dialog.setHeaderText("Lütfen varsayılan URL'nizi giriniz...");
        Stage stage = (Stage) dialog.getDialogPane().getScene().getWindow();
        stage.getIcons().add(image);

        Optional<String> result = dialog.showAndWait();

        result.ifPresent(name -> {
            defaultURL = result.get();
            for (Tab tab : tabPane.getTabs()) {
                Browser browser = (Browser) tab.getContent();
                browser.setDefaultURL(defaultURL);
            }
        });
    }
    public static void shortcuts(javafx.scene.image.Image image) {
        ButtonType ok = new ButtonType("Tamam", ButtonBar.ButtonData.OK_DONE);
        Alert alert = new Alert(Alert.AlertType.INFORMATION,"",ok);
        alert.setTitle("Ankara İnternet Tarayıcı");
        alert.setHeaderText("Kısa Yollar");
        alert.setContentText("Yeni Sekme: CTRL+T\nSekmeyi Kapat: CTRL+W\nÇıkış: CTRL+Q" +
                "\nGeçmiş: CTRL+H\nKısa Yollar: CTRL+R\nHakkında: CTRL+A" +
                "\nAna Sayfa Değiştir: CTRL+M\nİndirme Yolu: CTRL+D\n\nAnkara İnternet Tarayıcı, v0.1.0");
        Stage stage = (Stage) alert.getDialogPane().getScene().getWindow();
        stage.getIcons().add(image);
        alert.showAndWait();
    }

    public static void about(javafx.scene.image.Image image) {
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle("Ankara İnternet Tarayıcı");
        alert.setHeaderText("Hakkında");
        alert.setContentText("Ankara İnternet Tarayıcı, v0.1.0");
        Stage stage = (Stage) alert.getDialogPane().getScene().getWindow();
        stage.getIcons().add(image);
        alert.showAndWait();
    }

    public static void setDownloadPath(javafx.scene.image.Image image, String downloadPath,TabPane tabPane,Scene scene) {
        downloadFilePath=downloadPath;
        Dialog<String> dialog = new Dialog<>();
        dialog.setTitle("Varsayılan İndirme Yolu");
        dialog.setHeaderText("Lütfen varsayılan indirme yolunu giriniz...");
        Stage stage = (Stage) dialog.getDialogPane().getScene().getWindow();
        stage.getIcons().add(image);

        ButtonType selectButtonType = new ButtonType("Tamam", ButtonBar.ButtonData.OK_DONE);
        dialog.getDialogPane().getButtonTypes().addAll(selectButtonType, ButtonType.CANCEL);

        GridPane grid = new GridPane();
        grid.setHgap(10);
        grid.setVgap(10);
        grid.setPadding(new Insets(30, 30, 30, 30));

        TextField pathTextField = new TextField();
        pathTextField.setPrefWidth(300.0);
        pathTextField.setPromptText("İndirme Yolu");
        pathTextField.setText(downloadFilePath);

        File tmpdir = new File(downloadFilePath);
        if (!tmpdir.isDirectory()) downloadFilePath = "";

        Button changeButton = new Button("Seç");
        changeButton.setPrefWidth(100.0);
        changeButton.setOnAction(e -> {
            DirectoryChooser chooser = new DirectoryChooser();
            if (downloadFilePath != null && downloadFilePath.length() != 0) {
                try {
                    chooser.setInitialDirectory(new File(downloadFilePath));
                } catch (Exception er) {
                    chooser.setInitialDirectory(new File(System.getProperty("user.home")));
                }
            } else {
                chooser.setInitialDirectory(new File(System.getProperty("user.home")));
            }

            File initDir = chooser.showDialog(scene.getWindow());
            if (initDir != null)  	pathTextField.setText(initDir.getAbsolutePath());
        });

        grid.add(new Label("İndirme Yolu:"), 0, 0);
        grid.add(pathTextField, 1, 0);
        grid.add(changeButton, 2, 0);

        dialog.getDialogPane().setContent(grid);

        Platform.runLater(() -> pathTextField.requestFocus());

        dialog.setResultConverter(dialogButton -> {
            if (dialogButton == selectButtonType) {
                return pathTextField.getText();
            }
            return null;
        });

        Optional<String> result = dialog.showAndWait();

        result.ifPresent(name -> {
            File dir = new File(result.get());
            if (!dir.exists()) {
                Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
                alert.setTitle("Yeni Dizin");
                alert.setHeaderText("Yeni Dizin Oluştur");
                alert.setContentText("Dizin mevcut değil. Oluşturulsun mu?");

                Optional<ButtonType> resultAlert = alert.showAndWait();
                if (resultAlert.get() == ButtonType.OK){
                    Alert alertsub = new Alert(Alert.AlertType.ERROR);
                    try { dir.mkdirs(); } catch(Exception err) {
                        alertsub.setTitle("Hata!");
                        alertsub.setHeaderText("Dizin Oluşturma Hatası");
                        alertsub.setContentText("Dizin yüklenirken bir hata oluşur. Başka bir dizini seçtiniz.");
                        alertsub.showAndWait();
                        return;
                    }
                } else { return; }
            }
            if (!dir.canWrite()) {
                Alert alert = new Alert(Alert.AlertType.ERROR);
                alert.setTitle("Hata!");
                alert.setHeaderText("Dizin İzin Hatası");
                alert.setContentText("Yazılabilecek başka bir dizini seçmelisiniz.");
                alert.showAndWait();
                return;
            }
            downloadFilePath = result.get();
            for (Tab tab : tabPane.getTabs()) {
                Browser browser = (Browser) tab.getContent();
                browser.setDownloadPath(downloadFilePath);
            }
        });
    }

}
