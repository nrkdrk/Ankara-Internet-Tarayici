package com.ankara.tarayici.PaneAndPages;

import java.text.DecimalFormat;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.CustomMenuItem;
import javafx.scene.control.Label;
import javafx.scene.layout.HBox;
import javafx.scene.web.WebView;

/**
 * Created by Berk Can on 20.09.2018.
 */
public class ZoomMenuItem extends CustomMenuItem {

    private final Button plus, minus;
    private final Label name;
    private final HBox cont;

    public ZoomMenuItem(WebView web) {
        super(null, false);
        DecimalFormat df = new DecimalFormat("00");
        setContent(cont = new HBox(10, minus = new Button("-"),
                name = new Label("Mercek : " + df.format(web.getZoom() * 100) + "%"),
                plus = new Button("+")));
        cont.setAlignment(Pos.CENTER);
        setHideOnClick(false);
        web.zoomProperty().addListener((ob, older, newer) -> {
            if (newer != null) {
                name.setText("Zoom : " + df.format(newer.doubleValue() * 100) + "%");
            }
        });
        minus.setOnAction((e) -> {
            if (web.getZoom() > 0.2) {
                web.setZoom(web.getZoom() - 0.1);
            }
        });
        plus.setOnAction((e) -> {
            if (web.getZoom() < 3.0) {
                web.setZoom(web.getZoom() + 0.1);
            }
        });
    }
}