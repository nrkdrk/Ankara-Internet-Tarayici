package com.ankara.tarayici;

import com.ankara.tarayici.Models.Bookmark;
import com.ankara.tarayici.PaneAndPages.AlertDialogs;
import com.ankara.tarayici.tools.CreateLog;
import com.ankara.tarayici.tools.TabOperations;
import com.ankara.tarayici.tools.Tools;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyCodeCombination;
import javafx.scene.input.KeyCombination;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;

import java.io.*;
import java.security.Security;
import java.util.ArrayList;
import java.util.logging.Level;

/**
 * Developer Berk Can
 */

public class Main extends Application {

	private double stageX, stageY, stageWidth, stageHeight;
	private final String ICON_PATH = "/assignment3/pusula.png";
	private final String DEFAULT_FILE_PATH = "default.txt";
	private final String BOOKMARK_FILE_PATH = "bookmark.txt";
	public static String downloadPath, defaultURL,downloadsDirName;
	public static TabPane tabPane;
	public static Menu bookmarkMenu;
	public static ArrayList<Bookmark> bookmarks;
	public static javafx.scene.image.Image image;
	public static Tab tab;
	private Scene scene;

	public static void main(String[] args) throws Exception {
		Security.setProperty("ocsp.enable", "true");
		System.setProperty("com.sun.security.enableCRLDP", "true");
		System.setProperty("com.sun.net.ssl.checkRevocation", "true");
		launch(args);
	}

	@Override
	public void start(Stage primaryStage) throws Exception {
		bookmarks = new ArrayList<Bookmark>();
		defaultURL = "https://www.ankara.ilbilge.com";
		readSettings();
		createMainWindow();
		TabOperations.makeBookmarkMenu(tabPane,bookmarks,bookmarkMenu);

		primaryStage.setScene(scene);
		primaryStage.setTitle("Ankara İnternet Tarayıcı");
		image = new javafx.scene.image.Image(getClass().getResource("images/ankaratarayici.png").toExternalForm());
		primaryStage.getIcons().add(image);
		primaryStage.setX(stageX);
		primaryStage.setY(stageY);
		primaryStage.setOnCloseRequest(ev -> saveSettings());
		primaryStage.show();

		new CreateLog(Level.WARNING,"Main","start","Ankara İnternet Tarayıcı Başlatılıyor, pc Mac Adresi -> "+ Tools.getMacAddress());
	}

	private void createMainWindow() {
		final MenuItem newTab = new MenuItem("Yeni Sekme");
		newTab.setAccelerator(new KeyCodeCombination(KeyCode.E, KeyCombination.CONTROL_DOWN));
		newTab.setOnAction(e -> TabOperations.createNewTab(Main.tabPane,defaultURL,downloadPath));
		final MenuItem closeTab = new MenuItem("Sekmeyi Kapat");
		closeTab.setAccelerator(new KeyCodeCombination(KeyCode.W, KeyCombination.CONTROL_DOWN));
		closeTab.setOnAction(e -> TabOperations.closeThisTab(tabPane));
		final MenuItem quitMenu = new MenuItem("Çıkış");	// Quit Menu
		quitMenu.setAccelerator(new KeyCodeCombination(KeyCode.Q, KeyCombination.CONTROL_DOWN));
		quitMenu.setOnAction(e -> Platform.exit());
		final MenuItem homeMenu = new MenuItem("Anasayfa");	// Set Homepage URL
		homeMenu.setAccelerator(new KeyCodeCombination(KeyCode.M, KeyCombination.CONTROL_DOWN));
		homeMenu.setOnAction(e -> AlertDialogs.setHomepageURL(defaultURL,image,tabPane));
		final MenuItem downloadPathMenu = new MenuItem("İndirme Yolu"); // Set Download Path
		downloadPathMenu.setAccelerator(new KeyCodeCombination(KeyCode.D, KeyCombination.CONTROL_DOWN));
		downloadPathMenu.setOnAction(e -> AlertDialogs.setDownloadPath(image,downloadPath,tabPane,scene));
		final Menu fileMenu = new Menu("Dosya");
		fileMenu.getItems().addAll(newTab,closeTab, quitMenu);
		bookmarkMenu = new Menu("Sık Kullanılanlar");
		final Menu settingMenu = new Menu("Ayarlar");
		final Menu socialMenu = new Menu("Sosyal Medya");
		final MenuItem faceSocial = new MenuItem("Facebook");
		faceSocial.setOnAction(e -> TabOperations.createNewTab(Main.tabPane,"https://www.facebook.com/ankarabbld/?hc_ref=ARRldgqLtsnhxmiAMgiQTkMAfFbtk2QkUq-HVsvCGgsINP3sqk0SsK6WVgKjLt8LidI",downloadPath));
		final MenuItem twitterSocial = new MenuItem("Twitter");
		twitterSocial.setOnAction(e -> TabOperations.createNewTab(Main.tabPane,"https://twitter.com/ankarabbld",downloadPath));
		final MenuItem insSocial = new MenuItem("Instagram");
		insSocial.setOnAction(e -> TabOperations.createNewTab(Main.tabPane,"https://www.instagram.com/ankarabbld/",downloadPath));
		final MenuItem newSocial = new MenuItem("Haberler");
		newSocial.setOnAction(e -> TabOperations.createNewTab(Main.tabPane,"http://ankara.bel.tr/index.php/tools/blocks/pronews_list/rss?bID=4554&cID=198&arHandle=Main",downloadPath));
		socialMenu.getItems().addAll(faceSocial,twitterSocial,insSocial,newSocial);
		settingMenu.getItems().addAll(homeMenu, downloadPathMenu);
		final MenuBar menubar = new MenuBar(fileMenu, bookmarkMenu, settingMenu,socialMenu);
		tabPane = new TabPane();
		tabPane.getSelectionModel().selectedIndexProperty().addListener((observable, oldValue, newValue) -> {
			Tab tab = tabPane.getTabs().get((int) newValue);
		});
		final BorderPane root = new BorderPane();
		root.setTop(menubar);
		root.setCenter(tabPane);

		scene = new Scene(root, stageWidth, stageHeight);
		TabOperations.newTabPlusButton(tabPane);
	}

	private void readSettings() {
		ReadDefaultFile();
		ReadBookmarkFile();
	}

	private void ReadDefaultFile() {
		String line, attribute, value;
		File file = null;
		FileReader fileReader = null;
		BufferedReader buffReader = null;

		stageX = 50.0;
		stageY = 50.0;
		stageWidth = 800.0;
		stageHeight = 600.0;
		defaultURL = "https://www.ankara.ilbilge.com";

		downloadsDirName = "Ankara Tarayıcı Indirilenler";
		downloadPath = System.getProperty("user.home") + File.separator + downloadsDirName;
		File df = new File(downloadPath);
		if(!df.exists()){
			System.out.println("İndirme Klasörü Oluşturuluyor : " + downloadsDirName);
			try {
				new File(downloadPath).mkdirs();
			}
			catch (SecurityException se){
				se.printStackTrace();
			}
		}
		try {
			file = new File(DEFAULT_FILE_PATH);
			if (!file.isFile()) file.createNewFile();
		} catch (Exception error) { error.printStackTrace();}

		try {
			fileReader = new FileReader(DEFAULT_FILE_PATH);
			buffReader = new BufferedReader(fileReader);
			while((line = buffReader.readLine()) != null) {
				attribute = line.substring(0, line.indexOf("="));
				value = line.substring(line.indexOf("=") + 1);
				if (value != null) {
					//if (attribute.equals("screenX")) stageX = Double.parseDouble(value);
					//else if (attribute.equals("screenY")) stageY = Double.parseDouble(value);
					//else if (attribute.equals("width")) stageWidth = Double.parseDouble(value);
					//else if (attribute.equals("height")) stageHeight = Double.parseDouble(value);
					if (attribute.equals("homepage")) defaultURL = value;
					else if (attribute.equals("downloadDirectory")) downloadPath = value;
				}
			}
		} catch (Exception error) {
			error.printStackTrace();
		} finally {
			if (buffReader != null) try { buffReader.close(); } catch (IOException e) {}
			if (fileReader != null) try { fileReader.close(); } catch (IOException e) {}
		}
	}

	private void ReadBookmarkFile() {
		File file = null;
		FileInputStream fileInputStream = null;
		ObjectInputStream objInputStream = null;
		Bookmark line = null;

		try {
			file = new File(BOOKMARK_FILE_PATH);
			if (!file.isFile()) file.createNewFile();
		} catch (Exception error) { error.printStackTrace();}

		try {
			fileInputStream = new FileInputStream(BOOKMARK_FILE_PATH);
			objInputStream = new ObjectInputStream(fileInputStream);
			while((line = (Bookmark) objInputStream.readObject()) != null) {
				bookmarks.add(line);
			}
		} catch (EOFException error) {
		} catch (Exception error) {	
			error.printStackTrace();
		} finally {
			if (objInputStream != null) try { objInputStream.close(); } catch (IOException e) {}
			if (fileInputStream != null) try { fileInputStream.close(); } catch (IOException e) {}
		}
	}

	private void saveSettings() {
		writeDefaultFile();
		writeBookmarkFile();
	}

	private void writeDefaultFile() {
		FileWriter fileWriter = null;
		BufferedWriter buffWriter = null;

		try {
			fileWriter = new FileWriter(DEFAULT_FILE_PATH, false);
			buffWriter = new BufferedWriter(fileWriter);

			buffWriter.write("screenX=" + scene.getWindow().getX());
			buffWriter.newLine();
			buffWriter.write("screenY=" + scene.getWindow().getY());
			buffWriter.newLine();
			buffWriter.write("width=" + scene.getWidth());
			buffWriter.newLine();
			buffWriter.write("height=" + scene.getHeight());
			buffWriter.newLine();
			buffWriter.write("homepage=" + defaultURL);
			buffWriter.newLine();
			buffWriter.write("downloadDirectory=" + downloadPath);
			buffWriter.newLine();
		} catch (Exception error) {
			error.printStackTrace();
		} finally {
			if (buffWriter != null) try { buffWriter.close(); } catch (IOException e) {}
			if (fileWriter != null) try { fileWriter.close(); } catch (IOException e) {}
		}
	}

	private void writeBookmarkFile() {
		FileOutputStream fileOutputStream = null;
		ObjectOutputStream objOutputStream = null;

		try {
			fileOutputStream = new FileOutputStream(BOOKMARK_FILE_PATH);
			objOutputStream = new ObjectOutputStream(fileOutputStream);
			for (Bookmark bookmark : bookmarks) {
				objOutputStream.writeObject(bookmark);
			}
		} catch (Exception error) {
			error.printStackTrace();
		} finally {
			if (objOutputStream != null) try { objOutputStream.close(); } catch (IOException e) {}
			if (fileOutputStream != null) try { fileOutputStream.close(); } catch (IOException e) {}
		}
	}

}
