package com.ankara.tarayici.Events;

import java.util.EventObject;

import javafx.scene.image.ImageView;

/**
 * Developer Berk Can
 */

public class BookmarkActionEvent extends EventObject {

	private static final long serialVersionUID = 5718257236792830620L;
	private String title;
	private String url;
	private ImageView icon;

	public BookmarkActionEvent(Object source, String title, String url, ImageView icon) {
		super(source);
		this.title = title;
		this.url = url;
		this.icon = icon;
	}

	public String getTitle() { return title; }
	public String getUrl() { return url; }
	public ImageView getIcon() { return icon; }
}
