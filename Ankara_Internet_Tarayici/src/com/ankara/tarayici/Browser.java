package com.ankara.tarayici;

import com.ankara.tarayici.Interfaces.BookmarkActionListener;
import com.ankara.tarayici.PaneAndPages.AlertDialogs;
import com.ankara.tarayici.PaneAndPages.ZoomMenuItem;
import com.ankara.tarayici.tools.AutoCompleteTextField;
import com.ankara.tarayici.tools.CreateLog;
import javafx.animation.FadeTransition;
import javafx.animation.ParallelTransition;
import javafx.animation.SequentialTransition;
import javafx.animation.TranslateTransition;
import javafx.application.Platform;
import javafx.beans.binding.Bindings;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.concurrent.Worker;
import javafx.concurrent.Worker.State;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.geometry.VPos;
import javafx.scene.control.*;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuItem;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.*;
import javafx.scene.layout.*;
import javafx.scene.web.WebEngine;
import javafx.scene.web.WebHistory;
import javafx.scene.web.WebView;
import javafx.stage.Stage;
import javafx.util.Duration;
import netscape.javascript.JSObject;
import org.apache.commons.validator.routines.UrlValidator;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.SortedSet;
import java.util.logging.Level;

import static com.ankara.tarayici.tools.TabOperations.createNewTab;
import static com.ankara.tarayici.tools.TabOperations.createNewTabSettings;
import static com.ankara.tarayici.tools.Tools.historyListOpenAndClose;


/**
 * Developer Berk Can
 *
 * TEST
 */

public class Browser extends Region {

	//public static final String URL_REGEX = "^((https?|ftp)://|(www|ftp)\\.)?[a-z0-9-]+(\\.[a-z0-9-]+)+([/?].*)?$";
	public static java.util.List<WebEngine> webEngineList = new ArrayList<WebEngine>();
	private final double HISTORYLIST_WIDTH = 200.0;
	private boolean isHistoryListOpened;
	protected WebEngine engine;
	private String defaultURL;
	private String downloadPath;
	public static WebView browserView;
	private WebHistory history;
	private TextField urlText;
	private ListView<WebHistory.Entry> historyList;
	private Button backButton,refButton,homeButton, bookmarkButton, nextButton;
	private MenuButton menuButton;
	private BorderPane root;
	private java.util.List<BookmarkActionListener> bookmarkListeners;
	javafx.scene.image.Image image;
	Stage stage;

	public Browser(String defaultURL, String downloadPath) {
		this.defaultURL = defaultURL;
		this.downloadPath = downloadPath;
		image = new javafx.scene.image.Image(getClass().getResource("images/pusula.png").toExternalForm());

		browserView = new WebView();
		engine = browserView.getEngine();
        engine.getLoadWorker().stateProperty().addListener(new DownloadDetector());
		engine.getLoadWorker().stateProperty().addListener(new ChangeListener<State>() {
			@Override
			public void changed(ObservableValue<? extends State> ov, State oldState, State newState) {
				if (newState == State.SUCCEEDED) {
					JSObject win = (JSObject) engine.executeScript("window");
					win.setMember("app", new browserApp());
				}
			}
		});
		history = engine.getHistory();
		webEngineList.add(engine);
		bookmarkListeners = new ArrayList<BookmarkActionListener>();
		browserView.setContextMenuEnabled(false);

		createContextMenu(browserView);
		createScene();
		processForLoading();
		processForHistory();
	}

	private void createContextMenu(WebView webView) {
		ContextMenu contextMenu = new ContextMenu();
		MenuItem reload = new MenuItem("Sayfayı Yenile");
		reload.setOnAction(e -> webView.getEngine().reload());
		MenuItem savePage = new MenuItem("Sayfayı kaydet");
		savePage.setOnAction(e -> System.out.println("Save page..."));
		MenuItem hideImages = new MenuItem("Resimleri Gizle");
		hideImages.setOnAction(e -> System.out.println("Resimleri Gizle..."));
		MenuItem copy = new MenuItem("Kopyala");
		copy.setOnAction(e -> System.out.println("Kopyala"));
		contextMenu.getItems().addAll(reload, savePage, hideImages,copy);

		webView.setOnMousePressed(e -> {
			if (e.getButton() == MouseButton.SECONDARY) {
				contextMenu.show(webView, e.getScreenX(), e.getScreenY());
			} else {
				contextMenu.hide();
			}
		});
	}

	private void createScene() {
		MouseHandler myHandler = new MouseHandler();

		final Tooltip backButtonTooltip = new Tooltip();
		backButtonTooltip.setText(
				"Önceki Sayfa"
		);
		final Tooltip homeButtonTooltip = new Tooltip();
		homeButtonTooltip.setText(
				"Ana sayfa"
		);
		final Tooltip refButtonTooltip = new Tooltip();
		refButtonTooltip.setText(
				"Yenile"
		);
		final Tooltip nextButtonTooltip = new Tooltip();
		nextButtonTooltip.setText(
				"Sonraki Sayfa"
		);

		backButton = new Button();
		backButton.setGraphic(new ImageView(new Image(getClass().getResourceAsStream("/sources/left.png"))));
		backButton.disableProperty().bind(history.currentIndexProperty().lessThanOrEqualTo(0)); // Disable Condition
		backButton.setOnMouseClicked(myHandler);
		backButton.setTooltip(backButtonTooltip);

		homeButton = new Button();
		homeButton.setGraphic(new ImageView(new Image(getClass().getResourceAsStream("/sources/home.png"))));
		homeButton.setOnMouseClicked(myHandler);
		homeButton.setTooltip(homeButtonTooltip);
		homeButton.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent event) {
				engine.load(defaultURL);
			}
		});

		nextButton = new Button();
		nextButton.setGraphic(new ImageView(new Image(getClass().getResourceAsStream("/sources/right.png"))));

		nextButton.disableProperty().bind(
				history.currentIndexProperty().greaterThanOrEqualTo(Bindings.size(history.getEntries()).subtract(1)));
		nextButton.setOnMouseClicked(myHandler);
		nextButton.setTooltip(nextButtonTooltip);

		refButton = new Button();
		refButton.setGraphic(new ImageView(new Image(getClass().getResourceAsStream("/sources/refresh.png"))));
		refButton.setTooltip(refButtonTooltip);
		refButton.setOnAction(new EventHandler<ActionEvent>() {
			@Override public void handle(ActionEvent e) {
				engine.reload();
			}
		});

		BufferedImage image = new BufferedImage(25, 25, BufferedImage.TYPE_INT_RGB);
		Graphics graphics = image.getGraphics();
		graphics.setColor(Color.WHITE);
		graphics.fillRect(0, 0, 25, 25);
		graphics.setColor(Color.RED);
		graphics.fillRect(2, 11, 21, 3);
		graphics.fillRect(11, 2, 3, 21);

		urlText = new TextField();
		urlText.setOnMouseClicked(myHandler);

		urlText.setId("textField");
		urlText.setText(defaultURL);
		StackPane UrlLayout = new StackPane();
		UrlLayout.getChildren().addAll(urlText);
		UrlLayout.getStylesheets().add(this.getClass().getResource("textfield.css").toExternalForm());
		loadWebSite(defaultURL);
		urlText.setOnAction(a -> loadWebSite(urlText.getText()));
		new AutoCompleteTextField().bindAutoCompletion(urlText, 15, true, com.ankara.tarayici.tools.List.WEBSITE_PROPOSALS);

		final Tooltip bookmarkButtonTooltip = new Tooltip();
		bookmarkButtonTooltip.setText(
				"Sık Kullanılanlara Ekle"
		);

		bookmarkButton = new Button();
		bookmarkButton.setGraphic(new ImageView(new Image(getClass().getResourceAsStream("/sources/favorite.png"))));
		bookmarkButton.setDisable(true);
		bookmarkButton.setTooltip(bookmarkButtonTooltip);
		bookmarkButton.setOnMouseClicked(myHandler);

		menuButton = new MenuButton(null);
		menuButton.setGraphic(new ImageView(new Image(getClass().getResourceAsStream("/sources/settings.png"))));
		Menu menu=new Menu("Yardım");
		Menu bookMarkMenu=new Menu("Yer İşaretleri");

		MenuItem exitMenu = new MenuItem("Çıkış");
		MenuItem settingsMenu = new MenuItem("Ayarlar");
		MenuItem toolMenu = new MenuItem("Diğer Araçlar");
		MenuItem printMenu = new MenuItem("Yazdır");
		MenuItem downloadMenu = new MenuItem("İndirilenler");
		MenuItem historyMenu = new MenuItem("Geçmiş");
		MenuItem screenKeyboardMenu = new MenuItem("Ekran Klavyesi");
		MenuItem zoomMenu = new ZoomMenuItem(browserView);
		MenuItem secretTabMenu = new MenuItem("Yeni Gizli Pencere");
		MenuItem newScrennMenu = new MenuItem("Yeni Pencere");
		MenuItem newTabMenu = new MenuItem("Yeni Sekme");

		MenuItem informationMenu = new MenuItem("Ankara İnternet Tarayıcı Hakkında");
		MenuItem shortPathsMenu = new MenuItem("Kısa Yollar");
		MenuItem supportMenu = new MenuItem("Yardım Merkezi");
		MenuItem reportProblemMenu = new MenuItem("Sorun Bildir");

		menu.getItems().add(0,reportProblemMenu);
		menu.getItems().add(0,supportMenu);
		menu.getItems().add(0,shortPathsMenu);
		menu.getItems().add(0,informationMenu);

		newTabMenu.setAccelerator(new KeyCodeCombination(KeyCode.T, KeyCombination.CONTROL_DOWN));
		newScrennMenu.setAccelerator(new KeyCodeCombination(KeyCode.N, KeyCombination.CONTROL_DOWN));
		historyMenu.setAccelerator(new KeyCodeCombination(KeyCode.H, KeyCombination.CONTROL_DOWN));
		exitMenu.setAccelerator(new KeyCodeCombination(KeyCode.Q, KeyCombination.CONTROL_DOWN));
		informationMenu.setAccelerator(new KeyCodeCombination(KeyCode.A, KeyCombination.CONTROL_DOWN));
		shortPathsMenu.setAccelerator(new KeyCodeCombination(KeyCode.R, KeyCombination.CONTROL_DOWN));

		menuButton.getItems().add(0,exitMenu);
		menuButton.getItems().add(0,settingsMenu);
		menuButton.getItems().add(0,menu);
		menuButton.getItems().add(0,toolMenu);
		menuButton.getItems().add(0,printMenu);
		menuButton.getItems().add(0,downloadMenu);
		menuButton.getItems().add(0,bookMarkMenu);
		menuButton.getItems().add(0,historyMenu);
		menuButton.getItems().add(0,screenKeyboardMenu);
		menuButton.getItems().add(0,zoomMenu);
		menuButton.getItems().add(0,secretTabMenu);
		menuButton.getItems().add(0,newScrennMenu);
		menuButton.getItems().add(0,newTabMenu);

		bookMarkMenu.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent event) {
				new CreateLog(Level.WARNING,"Browser","menuItem1.setOnAction","Ankara İnternet Tarayıcıda yeni sekme açıldı ");
				createNewTab(Main.tabPane,defaultURL,downloadPath);
			}
		});

		newScrennMenu.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent event) {
				System.out.println("Option 2 selected");
			}
		});

		secretTabMenu.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent event) {
				System.out.println("Option 3 selected");
			}
		});

		historyMenu.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent event) {
				new CreateLog(Level.WARNING,"Browsser","menuItem4.setOnAction","Geçmiş ");
				historyListOpenAndClose(Main.tabPane);
			}
		});

		downloadMenu.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent event) {
				System.out.println("Option 5 selected");
			}
		});

		printMenu.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent event) {
				System.out.println("Option 7 selected");
			}
		});

		toolMenu.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent event) {
			}
		});

		settingsMenu.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent event) {
				new CreateLog(Level.WARNING,"Browsser","menuItem9.setOnAction","Çıkış Yapılıyor ");
				createNewTabSettings(Main.tabPane,defaultURL,downloadPath);
			}
		});

		exitMenu.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent event) {
				new CreateLog(Level.WARNING,"Browsser","menuItem9.setOnAction","Çıkış Yapılıyor ");
				Platform.exit();
			}
		});

		informationMenu.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent event) {
				AlertDialogs.about(Main.image);
				new CreateLog(Level.WARNING,"Browsser","menuItem1_1.setOnAction","Hakkında");
			}
		});

		shortPathsMenu.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent event) {
				AlertDialogs.shortcuts(Main.image);
				new CreateLog(Level.WARNING,"Browsser","menuItem1_2.setOnAction","Kısayollar");
			}
		});

		supportMenu.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent event) {
				System.out.println("Option 9 selected");
			}
		});

		reportProblemMenu.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent event) {
				System.out.println("Option 9 selected");
			}
		});

		HBox browserBar = new HBox(backButton,homeButton,nextButton,refButton,urlText,bookmarkButton,menuButton);
		HBox.setHgrow(urlText, Priority.ALWAYS);

		historyList = new ListView<>();
		historyList.setItems(history.getEntries());
		historyList.setMaxWidth(0.0);
		isHistoryListOpened = false;

		browserView.setOnKeyPressed(new EventHandler<KeyEvent>() {
			@Override
			public void handle(KeyEvent ke) {
				if (ke.isControlDown()) {
					if (ke.getCode() == KeyCode.LEFT && !backButton.isDisable()) { // history back
						goBack();
					} else if (ke.getCode() == KeyCode.RIGHT && !nextButton.isDisable()) { // history forward
						goForward();
					}
				}
				if(ke.getCode() == KeyCode.F5){
					engine.reload();
				}
			}
		});

		root = new BorderPane();
		root.setPadding(new Insets(3, 2, 2, 2));
		root.setTop(browserBar);
		root.setCenter(browserView);
		root.setRight(historyList);

		getChildren().add(root);
	}

	@Override
	protected void layoutChildren() {
		layoutInArea(root, 0.0, 0.0, getWidth(), getHeight(), 0.0, HPos.CENTER, VPos.CENTER);
	}

	public void goBack() { history.go(-1); }

	public void goForward() { history.go(1); }

	public void goURL(String URL) { engineLoadAndControl(URL); }

	public void setDefaultURL(String URL) { defaultURL = URL; }

	public void setDownloadPath(String Path) { downloadPath = Path; }

	public void executeJavaScript(String command) {
		engine.executeScript(command);
	}

	public synchronized void addBookmarkListener(com.ankara.tarayici.Interfaces.BookmarkActionListener event) {
		bookmarkListeners.add(event);
	}

	public synchronized void removeBookmarkListener(com.ankara.tarayici.Interfaces.BookmarkActionListener event) {
		bookmarkListeners.remove(event);
	}

	private synchronized void processForLoading() {
		engine.getLoadWorker().stateProperty().addListener((ov, oldState, newState) -> {
			if (newState == State.SUCCEEDED) {
				TabPane tabs = (TabPane) getParent().getParent();
				Tab tab = tabs.getTabs().get(tabs.getSelectionModel().getSelectedIndex());
				tab.setText(engine.getTitle());
				try {
					tab.setGraphic(loadFavicon(engine.getDocument().getBaseURI()));
				}catch (Exception e){

				}
				String url = engine.getLocation();
				urlText.setText(url);
				fireBookmarkEvent(1);
			}
		});

		engine.locationProperty().addListener(new ChangeListener<String>() {
			@Override
			public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
				String downloadableFile = null;
				String[] downloadableFiles = { ".doc", ".docx", ".xls", ".xlsx", ".iso",
						".zip", ".tgz", ".jar",".pdf", ".exe", ".img", ".dmg", ".tar" };
				for (String extension : downloadableFiles) {
					if (newValue.endsWith(extension)) {
						downloadableFile = extension;
						break;
					}
				}
				if (downloadableFile != null) {
					int filenameIndex = newValue.lastIndexOf("/") + 1; // if file name is existed?
					if (filenameIndex != 0) {
						try { new com.ankara.tarayici.tools.DownloadBar(newValue, downloadPath); } catch (Exception e) {};
					}
				}
			}
		});

		engine.setOnAlert(ev -> {
			Alert alert = new Alert(AlertType.ERROR);
			alert.setTitle("Uyarı");
			alert.setHeaderText("JavaScript uyarı Mesajı");
			alert.setContentText(engine.getTitle());
			alert.showAndWait();
		});
	}

	private void processForHistory() {
		historyList.setCellFactory(lv -> new ListCell<WebHistory.Entry>() {
			@Override
			public void updateItem(WebHistory.Entry entry, boolean empty) {
				super.updateItem(entry, empty);
				if (empty) {
					setText(null);
					setGraphic(null);
				} else {
					textProperty().set((entry.getTitle() == null ) ? entry.getUrl() : entry.titleProperty().get());
					graphicProperty().setValue(loadFavicon(entry.getUrl()));
				}
			}
		});

		history.currentIndexProperty().addListener((obs, oldIndex, newIndex) -> {
			if (newIndex.intValue() != historyList.getSelectionModel().getSelectedIndex()) {
				historyList.getSelectionModel().clearAndSelect(newIndex.intValue());
			}
		});

		historyList.getSelectionModel().selectedIndexProperty().addListener((obs, oldValue, newValue) -> {
			if (newValue.intValue() != history.getCurrentIndex()) {
				history.go(newValue.intValue() - history.getCurrentIndex());
			}
		});
	}

	private class MouseHandler implements EventHandler<MouseEvent> {
		@Override
		public void handle(MouseEvent e) {
			if (e.getSource().equals(backButton)) {
				if (e.getClickCount() == 1)	goBack();
			} else if (e.getSource().equals(urlText)) {
				if (e.getClickCount() == 2)
					urlText.setText("");
				else if (e.getClickCount() == 1)
					urlText.selectAll();
			} else if (e.getSource().equals(bookmarkButton)) {
				if (e.getClickCount() == 1) {
					addBookmarks();
				}
			} else if (e.getSource().equals(nextButton)) {
				if (e.getClickCount() == 1)	goForward();
			}
			e.consume();
		}
	}

	public void animationListDoor() {
		if (!isHistoryListOpened) {
			historyList.setMaxWidth(HISTORYLIST_WIDTH);

			FadeTransition ft = new FadeTransition(Duration.millis(500.0));
			ft.setByValue(0.3);
			ft.setCycleCount(2);
			ft.setAutoReverse(true);

			TranslateTransition tt = new TranslateTransition(Duration.millis(10.0));
			tt.setFromX(0.0);
			tt.setToX(20.0);
			tt.setCycleCount(50);
			tt.setAutoReverse(true);

			ParallelTransition pt = new ParallelTransition(historyList, ft,  tt);
			pt.play();
			isHistoryListOpened = true;
		} else {
			TranslateTransition t1 = new TranslateTransition(Duration.millis(10.0));
			t1.setByX(0.0);
			t1.setCycleCount(1);
			t1.setAutoReverse(true);

			TranslateTransition t2 = new TranslateTransition(Duration.millis(10.0));
			t2.setFromX(0.0);
			t2.setToX(20.0);
			t2.setCycleCount(50);
			t2.setAutoReverse(true);

			TranslateTransition t3 = new TranslateTransition(Duration.millis(500.0));
			t3.setByX(HISTORYLIST_WIDTH);
			t3.setCycleCount(1);
			t3.setAutoReverse(true);

			SequentialTransition st = new SequentialTransition (historyList, t1, t2, t3);
			st.setOnFinished(event -> {
				historyList.setMaxWidth(0.0);
				isHistoryListOpened = false;
			});
			st.play();
		}
	}

	public void loadWebSite(String webSite) {
		String load = !new UrlValidator().isValid(webSite) ? null : webSite;
		try {
			String finalWebsiteFristPart = ( load != null ) ? load : "https://www.ankara.ilbilge.com";
			String finalWebsiteSecondPart = "";
			if (urlText.getText().isEmpty()){
				finalWebsiteSecondPart = "";
			}else if(urlText.getText().contains("https://www.")){
				engineLoadAndControl(webSite);
				new com.ankara.tarayici.tools.CreateLog(Level.WARNING,"Browser","loadWebSite",webSite+" adresine gidildi");
			}else if(urlText.getText().contains("https://")){
				engineLoadAndControl(webSite);
				new com.ankara.tarayici.tools.CreateLog(Level.WARNING,"Browser","loadWebSite",webSite+" adresine gidildi");
			}else if(urlText.getText().contains("http://www.")){
				engineLoadAndControl(webSite);
				new com.ankara.tarayici.tools.CreateLog(Level.WARNING,"Browser","loadWebSite",webSite+" adresine gidildi");
			}else if(urlText.getText().contains("http://")){
				engineLoadAndControl(webSite);
				new com.ankara.tarayici.tools.CreateLog(Level.WARNING,"Browser","loadWebSite",webSite+" adresine gidildi");
			}else if(urlText.getText().contains("www.")){
				engineLoadAndControl("http://"+webSite);
				new com.ankara.tarayici.tools.CreateLog(Level.WARNING,"Browser","loadWebSite","http://"+webSite+" adresine gidildi");
			}else if(isContaining(com.ankara.tarayici.tools.List.webSiteExt,urlText.getText().toString())){
				engineLoadAndControl("http://www."+webSite);
				new com.ankara.tarayici.tools.CreateLog(Level.WARNING,"Browser","loadWebSite","http://www."+webSite+" adresine gidildi");
			}else{
				finalWebsiteSecondPart = "/ara.php?url=" + URLEncoder.encode(webSite, "UTF-8");
				engineLoadAndControl(finalWebsiteFristPart + finalWebsiteSecondPart);
				new com.ankara.tarayici.tools.CreateLog(Level.WARNING,"Browser","loadWebSite",finalWebsiteFristPart + finalWebsiteSecondPart+" adresine gidildi");
			}
		} catch (UnsupportedEncodingException ex) {
			ex.printStackTrace();
		}
	}

	void engineLoadAndControl(String url){
		if(urlText.getText().endsWith(".pdf")){
			//buraya pdf okuma eklenecek
            engine.load(url);

		} else{
			engine.load(url);
		}
		engine.getLoadWorker().stateProperty().addListener(new ChangeListener<State>() {
			@Override
			public void changed(ObservableValue<? extends State> ov, State t, State t1) {
				if (t1 == Worker.State.SUCCEEDED) {
					new com.ankara.tarayici.tools.CreateLog(Level.WARNING,"Browser","engineLoadAndControl","Url Başarılı Şekilde Yüklendi "+engine.getLocation().toString());
				}else if(t1 == State.CANCELLED){
					new com.ankara.tarayici.tools.CreateLog(Level.WARNING,"Browser","engineLoadAndControl","Url Yüklenemedi "+engine.getLocation().toString());
				}
			}
		});
	}

	private boolean isContaining(SortedSet sset, String str){
		long start= 0;
		long stop = 0;
		start = System.nanoTime();
		Iterator it = sset.iterator();
		String nxt;
		while (it.hasNext()) {
			nxt = it.next().toString();
			if (str.lastIndexOf(nxt)>0) {
				stop = System.nanoTime();
				System.out.println("Took :" + (stop-start) + " nano seconds");
				return true;
			}
		}
		return false;
	}

	public ImageView loadFavicon(String location) {
		ImageView favicon = null;
		try {
			String faviconUrl = String.format("http://www.google.com/s2/favicons?domain_url=%s", URLEncoder.encode(location, "UTF-8"));
			favicon = new ImageView(new Image(faviconUrl, true));
		} catch (UnsupportedEncodingException ex) {
		}
		return favicon;
	}

	private synchronized void addBookmarks() {
		fireBookmarkEvent(0);
		bookmarkButton.setDisable(true);
		bookmarkButton.setGraphic(new ImageView(new Image(getClass().getResourceAsStream("/sources/favorite-gold.png"))));
	}

	private synchronized void fireBookmarkEvent(int whichEvent) {
		String title = engine.getTitle();
		String location = engine.getLocation();
		if (title == null || title == "") title = location;
		ImageView icon = null;
		try {
			icon = loadFavicon(engine.getDocument().getBaseURI());
		}catch (Exception e){

		}
		com.ankara.tarayici.Events.BookmarkActionEvent event = new com.ankara.tarayici.Events.BookmarkActionEvent(this, title, location, icon);
		Iterator<com.ankara.tarayici.Interfaces.BookmarkActionListener> listeners = bookmarkListeners.iterator();
		while( listeners.hasNext() ) {
			if (whichEvent == 0)
				(listeners.next()).onBookmarkButtonClick(event);
			else bookmarkButton.setDisable((listeners.next()).isBookmarkThere(event));
		}
	}

	public class browserApp {
		public void exit() {
			Platform.exit();
		}

		public void log(String s) {
			System.out.println(s);
		}
	}

	public class DownloadDetector implements ChangeListener<State> {
		@Override
		public void changed(ObservableValue<? extends State> observable , State oldState , State newState) {
			if (newState == Worker.State.CANCELLED) {
				String url = engine.getLocation();
			}
		}
	}

}
