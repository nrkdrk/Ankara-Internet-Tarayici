package com.ankara.tarayici.tools;

import com.ankara.tarayici.Browser;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.scene.control.*;

import java.util.ArrayList;
import java.util.logging.Level;

/**
 * Created by Berk Can on 16.09.2018.
 */
public class TabOperations {

    public Tab currentTab;

    public static void closeThisTab(TabPane tabPane){

        int index = tabPane.getSelectionModel().getSelectedIndex();
        Browser.webEngineList.get(index-1).load(null);
        tabPane.getTabs().remove(tabPane.getSelectionModel().getSelectedIndex());
        Browser.webEngineList.remove(index-1);
        new CreateLog(Level.WARNING,"TabOperations","closeThisTab","Ankara İnternet Tarayıcısında bir sekme kapatıldı  ");

    }

    public static void createNewTabSettings(TabPane tabPane,String defaultURL,String downloadPath) {
        Tab tab = new Tab("Ayarlar");
        if (tabPane.getTabs().size() == 0) tab.setClosable(false);
        com.ankara.tarayici.Settings settings=new com.ankara.tarayici.Settings(defaultURL,downloadPath);

        tab.setContent(settings);
        tabPane.getTabs().add(tab);
        tabPane.getSelectionModel().select(tab);
        new CreateLog(Level.WARNING,"TabOperations","createNewTabSettings","Ankara İnternet Tarayıcısında ayarlar sayfası açıldı  ");
    }

    public static void createNewTab(TabPane tabPane,String defaultURL,String downloadPath) {
        Tab tab = new Tab("Yeni Sekme");
        if (tabPane.getTabs().size() == 0) tab.setClosable(false);
        com.ankara.tarayici.Browser browser = new com.ankara.tarayici.Browser(defaultURL, downloadPath);

        tab.setContent(browser);
        tabPane.getTabs().add(tab);
        tabPane.getSelectionModel().select(tab);
    }

    public static void makeBookmarkMenu(TabPane tabPane, ArrayList<com.ankara.tarayici.Models.Bookmark> bookmarks, Menu bookmarkMenu) {
        com.ankara.tarayici.Browser browser = (com.ankara.tarayici.Browser) tabPane.getTabs().get(tabPane.getSelectionModel().getSelectedIndex()).getContent();
        for (com.ankara.tarayici.Models.Bookmark bookmark : bookmarks) {
            MenuItem bookmarkMenuItem = new MenuItem(bookmark.getTitle(), browser.loadFavicon(bookmark.getUrl()));
            bookmarkMenuItem.setOnAction(action -> Tools.loadBookmark(bookmark.getUrl(),tabPane));
            bookmarkMenu.getItems().add(bookmarkMenuItem);
        }
    }

    public static void createNewBrowserTab(Tab tab, TabPane tabPane, String defaultURL, String downloadPath, ArrayList<com.ankara.tarayici.Models.Bookmark> bookmarks, Menu bookmarkMenu) {
        tab = new Tab("Ankara İnternet Tarayıcı");
        if (tabPane.getTabs().size() == 0){
            tab.setClosable(false);
        }
        com.ankara.tarayici.Browser browser = new com.ankara.tarayici.Browser(defaultURL, downloadPath);
        browser.addBookmarkListener(new com.ankara.tarayici.Interfaces.BookmarkActionListener() {
            @Override
            public void onBookmarkButtonClick(com.ankara.tarayici.Events.BookmarkActionEvent actionEvent) {
                bookmarks.add(new com.ankara.tarayici.Models.Bookmark(actionEvent.getTitle(), actionEvent.getUrl()));
                MenuItem bookmarkMenuItem = new MenuItem(actionEvent.getTitle(), actionEvent.getIcon());
                bookmarkMenuItem.setOnAction(action -> Tools.loadBookmark(actionEvent.getUrl(),tabPane));
                bookmarkMenu.getItems().add(bookmarkMenuItem);
            }
            @Override
            public boolean isBookmarkThere(com.ankara.tarayici.Events.BookmarkActionEvent actionEvent) {
                for (com.ankara.tarayici.Models.Bookmark bookmark : bookmarks) {
                    if (actionEvent.getUrl().equals(bookmark.getUrl())) {
                        return true;
                    }
                }
                return false;
            }
        });
        //tab.getOnClosed().handle(null);

        tab.setOnCloseRequest(new EventHandler<Event>() {
            @Override
            public void handle(Event t) {

                closeThisTab(tabPane);
                t.consume();
            }
        });
        tab.setContent(browser);
        tabPane.getTabs().add(tab);
        tabPane.getSelectionModel().select(tab);
    }

    public static void newTabPlusButton(TabPane tabPane){
        Tab tab=new Tab();
        tab.setClosable(false);
        Label addLabel = new Label("\u2795");
        tab.setOnSelectionChanged(new EventHandler<Event>() {
            @Override
            public void handle(Event t) {
                if (tab.isSelected()) {
                    createNewBrowserTab(com.ankara.tarayici.Main.tab, com.ankara.tarayici.Main.tabPane, com.ankara.tarayici.Main.defaultURL, com.ankara.tarayici.Main.downloadPath, com.ankara.tarayici.Main.bookmarks, com.ankara.tarayici.Main.bookmarkMenu);
                }
            }
        });

        tab.setGraphic(addLabel);

        /*
        tab.setOnCloseRequest(new EventHandler<Event>() {
            @Override
            public void handle(Event t) {
                System.out.println("OH SHIT ITS CLOSED!");
            }
        });
        */
        tabPane.getTabs().add(tab);

        new CreateLog(Level.WARNING,"TabOperations","newTabPlusButton","Ankara İnternet Tarayıcısında yeni sekme açıldı");

    }

}
