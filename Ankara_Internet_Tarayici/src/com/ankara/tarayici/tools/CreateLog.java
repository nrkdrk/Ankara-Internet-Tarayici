package com.ankara.tarayici.tools;


import java.util.logging.Level;

/**
 * Created by Berk Can on 9.09.2018.
 */
public class CreateLog {

    static String className;
    static String metodName;
    static String logDetail;
    static Level level;

    public CreateLog(Level level,String className,String metodName, String logDetail){
        this.className=className;
        this.metodName=metodName;
        this.level=level;
        this.logDetail=logDetail;
        createLog();
    }

    public static void createLog(){
        System.out.println("Anadolu Log: Class -> "+className+", Metod -> "+metodName+" -> "+logDetail);
    }

}
