package com.ankara.tarayici.tools;

import java.awt.Desktop;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Optional;

import javafx.animation.FadeTransition;
import javafx.animation.ParallelTransition;
import javafx.animation.SequentialTransition;
import javafx.animation.TranslateTransition;
import javafx.concurrent.Task;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Label;
import javafx.scene.control.ProgressBar;
import javafx.scene.control.ProgressIndicator;
import javafx.scene.control.TextArea;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;
import javafx.scene.media.AudioClip;
import javafx.stage.Stage;
import javafx.util.Duration;

/**
 * Developer Berk Can
 */
public class DownloadBar extends BorderPane{


	private static Stage downloadStage = null;
	private static VBox downloadVBox;
	private static TextArea statusTextArea;
	private Label filenameLabel;
	private ProgressBar progBar;
	private ProgressIndicator progIndicator;
	private Button cancelButton;
	private HBox downloadRoot;
	private String sourceURL;
	private String destFullName;
	private String destFileName;
	private DownloadTask downloadTask;

	public Stage getDownloadWindow() {
		if(downloadStage == null) {

			downloadVBox = new VBox();
			BorderPane root = new BorderPane();
			statusTextArea = new TextArea();
			statusTextArea.setEditable(false);
			statusTextArea.setWrapText(true);
			statusTextArea.setPrefHeight(300.0);
			root.setCenter(new VBox(downloadVBox, statusTextArea));
			Button closeButton = new Button("Kapat");
			closeButton.setOnAction(e -> downloadStage.close());
			closeButton.setPrefWidth(80.0);
			root.setBottom(closeButton);
			BorderPane.setAlignment(closeButton, Pos.CENTER_RIGHT);
			BorderPane.setMargin(closeButton, new Insets(10, 10, 10, 10));

		}
		return downloadStage;
	}

	private HBox getDownloadTasksWindow() {
		filenameLabel = new Label(destFileName);
		filenameLabel.setPrefWidth(100.0);
		filenameLabel.setAlignment(Pos.CENTER_RIGHT);
		
		progBar = new ProgressBar();
		progBar.setPrefWidth(200.0);
		
		progIndicator = new ProgressIndicator();
		progIndicator.setPrefHeight(100.0);
		
		cancelButton = new Button("Vazgeç");
		cancelButton.setPrefWidth(70.0);
		cancelButton.setOnAction(eh -> {
			Alert alert = new Alert(AlertType.CONFIRMATION);
            alert.setTitle("Onaylamalısın...");
            alert.setHeaderText(null);
            alert.setContentText("Bu indirme işlemini iptal etmek istediğinize emin misiniz?");

            Optional<ButtonType> result = alert.showAndWait();
            if (result.get() == ButtonType.OK){
            	downloadTask.cancel();
            } 
		});
		
		downloadRoot = new HBox(filenameLabel, progBar, progIndicator, cancelButton);
		downloadRoot.setPrefHeight(HBox.USE_COMPUTED_SIZE);
		downloadRoot.setPrefWidth(HBox.USE_COMPUTED_SIZE);
		downloadRoot.setAlignment(Pos.CENTER);
		downloadRoot.setStyle("-fx-border-width: 1px; -fx-border-color: darkgrey;");
		downloadRoot.setPadding(new Insets(15, 3, 3, 3));
		downloadRoot.setSpacing(10.0);
		HBox.setHgrow(progBar, Priority.ALWAYS);
		
		return downloadRoot;
	}

	public DownloadBar(String sourceURL, String destinationPath) throws Exception {
		
		this.sourceURL = sourceURL;
		try {
			getDownloadWindow().show();
		}
		catch (Exception e){}
		URL url = new URL(sourceURL);
        HttpURLConnection httpConn = (HttpURLConnection) url.openConnection();
        int responseCode = httpConn.getResponseCode();
        int contentLength = -1;

        if (responseCode == HttpURLConnection.HTTP_OK) {
            String fileName = sourceURL.substring(sourceURL.lastIndexOf("/") + 1,
                    sourceURL.length());
            String disposition = httpConn.getHeaderField("Content-Disposition");
            contentLength = httpConn.getContentLength();
 
            if (disposition != null) {
                int index = disposition.indexOf("filename=");
                if (index > 0) {
                    fileName = disposition.substring(index + 10, disposition.length() - 1);
                }
            }

	      	String fileExtension = fileName.substring(fileName.lastIndexOf("."));
	      	if (fileExtension == null) fileExtension = "";
	      	fileName = fileName.substring(0, fileName.lastIndexOf("."));

	      	int count = 0;
	      	while (true) {
	      		String numFileName = fileName + ((count == 0) ? "" : "(" + count + ")");
	      		String fullFileName = destinationPath + File.separator + numFileName + fileExtension;
	      		File file = new File(fullFileName);
	      		if (!file.isFile()) {
	      			fileName = numFileName;
	      			break;
	      		}
	      		count++;
	      	}
	      	destFileName = fileName + fileExtension;
	      	destFullName = destinationPath + File.separator + destFileName;
        }
        
        httpConn.disconnect();
	      	
        downloadVBox.getChildren().add( getDownloadTasksWindow() );
        downloadTask = new DownloadTask( contentLength );
        downloadTask.setOnRunning(eh -> statusTextArea.appendText( sourceURL + "dosyası indiriliyor.\n"));
        downloadTask.setOnSucceeded(eh -> {
			statusTextArea.appendText(sourceURL + " Dosyasını indirme başarıyla tamamlandı.\n");
			closeAnimation();
			Alert alert = new Alert(AlertType.CONFIRMATION);
			alert.setTitle("Dosya indirildi");
			alert.setHeaderText(null);
			alert.setContentText("İndirilen dosyayı açalım mı?");

			Optional<ButtonType> result = alert.showAndWait();
			if (result.get() == ButtonType.OK){
				try { Desktop.getDesktop().open(new File(destFullName)); } catch (Exception err) {
					Alert iuAlert = new Alert(AlertType.INFORMATION); // indirme uyarı alerti
					iuAlert.setTitle("Hata");
					iuAlert.setHeaderText(null);
					iuAlert.setContentText("Dosya açılamadı.");
				}
			} 
		});

        downloadTask.setOnFailed(eh -> {
			statusTextArea.appendText( sourceURL + " Dosyasını indirme esnasında bilinmeyen bir hata oluştu.\n" );
			Alert alert = new Alert(AlertType.INFORMATION);
			alert.setTitle("İndirme başarısız oldu");
			alert.setHeaderText(null);
			alert.setContentText(sourceURL + " Başarısız.\n");
			alert.showAndWait();
			closeAnimation();
			File delFile = new File(destFullName);
			delFile.delete();
		});

        downloadTask.setOnCancelled(eh -> {
			statusTextArea.appendText(sourceURL + " indirme iptal edildi.\n" );
			closeAnimation();
			File delFile = new File(destFullName);
			delFile.delete();
		});
		
        progBar.progressProperty().bind(downloadTask.progressProperty());
        progIndicator.progressProperty().bind(downloadTask.progressProperty());

        Thread thread = new Thread(downloadTask);
        thread.setDaemon(true);
        thread.start();
	}

	private void closeAnimation() {
		TranslateTransition tt1 = new TranslateTransition(Duration.millis(10.0));
		tt1.setFromY(0.0);
		tt1.setToY(10.0);
		tt1.setCycleCount(50);
		tt1.setAutoReverse(true);

		FadeTransition ft = new FadeTransition(Duration.millis(500.0));
		ft.setByValue(0.3);
		ft.setCycleCount(2);
		ft.setAutoReverse(true);

		TranslateTransition tt2 = new TranslateTransition(Duration.millis(500.0));
		tt2.setFromY(0.0);
		tt2.setToY(50.0);
		tt2.setCycleCount(1);

		ParallelTransition pt = new ParallelTransition(ft,  tt2);

		SequentialTransition st = new SequentialTransition (downloadRoot, tt1, pt);
		st.setOnFinished(event -> {
			AudioClip plonkSound = new AudioClip("http://www.flashkit.com/imagesvr_ce/flashkit/soundfx/Interfaces/Click_So-S_Bainbr-7969/Click_So-S_Bainbr-7969_hifi.mp3");
			plonkSound.play();
			downloadVBox.getChildren().remove(downloadRoot);
		});

		st.play();
	}

	private class DownloadTask extends Task<String> {
		private static final int BUFFER_SIZE = 4096;
		private int contentLength;
		
		private DownloadTask( int contentLength ) {
			this.contentLength = contentLength;
		}
		
		@Override
		protected String call() throws Exception {
			URL url = new URL(sourceURL);
	        HttpURLConnection httpConn = (HttpURLConnection) url.openConnection();
	        int responseCode = httpConn.getResponseCode();
	        
	        if (responseCode == HttpURLConnection.HTTP_OK) {
	            InputStream inputStream = httpConn.getInputStream();
	            
	            FileOutputStream outputStream = new FileOutputStream(destFullName);
	 
	            long nread = 0L;
	            int bytesRead = -1;
	            byte[] buffer = new byte[BUFFER_SIZE];
	            while ((bytesRead = inputStream.read(buffer)) != -1) {
	                outputStream.write(buffer, 0, bytesRead);
	                nread += bytesRead;
	                updateProgress(nread, contentLength);
	                if (isCancelled()) {
	                    break;
	                }
	            }
	            outputStream.close();
	            inputStream.close();
	        } 
	        httpConn.disconnect();
			return "Finished";
		}
		
		@Override
		protected void succeeded() {
			super.succeeded();	
		}
		
		@Override
		protected void cancelled() {
			super.cancelled();
		}
		
		@Override
		protected void failed() {		
			super.failed();	
		}
	}		
}
