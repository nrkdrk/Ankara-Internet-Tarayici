package com.ankara.tarayici.tools;

import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;

import java.io.*;
import java.net.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.logging.Level;

import static java.nio.file.StandardOpenOption.APPEND;
import static java.nio.file.StandardOpenOption.CREATE;

/**
 * Created by Berk Can on 9.09.2018.
 */
public class Tools {

    public static String[] webSiteExt;

    public static String getMacAddress()
            throws UnknownHostException, SocketException {
        InetAddress ip = InetAddress.getLocalHost();
        NetworkInterface ni = NetworkInterface.getByInetAddress(ip);
        if (ni == null)
            return null;

        byte[] mac = ni.getHardwareAddress();
        if (mac == null)
            return null;

        StringBuilder sb = new StringBuilder(18);
        for (byte b : mac) {
            if (sb.length() > 0)
                sb.append(':');
            sb.append(String.format("%02x", b));
        }
        return sb.toString();
    }

    public static void fileCreator(String filePath,String fileNameAndType,String fileDescription){
        String s = fileDescription;
        byte data[] = s.getBytes();
        Path file = Paths.get(filePath, fileNameAndType);

        try (OutputStream out = new BufferedOutputStream(
                Files.newOutputStream(file, CREATE, APPEND))) {
            out.write(data, 0, data.length);
            new CreateLog(Level.WARNING,"Tools","fileCreator",fileNameAndType+" isimli dosya belirtilen yolda oluşturldu --->"+" "+file);
        } catch (IOException x) {
            new CreateLog(Level.WARNING,"Tools","fileCreator","Dosya oluşturma esnasında hata oluştu --->"+" "+x);
            System.err.println(x);
        }
    }

    public static void fileReader(String fileName){
        String line = null;
        try {
            FileReader fileReader =
                    new FileReader(fileName);
            BufferedReader bufferedReader =
                    new BufferedReader(fileReader);
            while((line = bufferedReader.readLine()) != null) {
                System.out.println(line);
            }
            bufferedReader.close();
        }
        catch(FileNotFoundException ex) {
            System.out.println("Dosya açılamıyor '" + fileName + "'");
        }catch(IOException ex) {
            System.out.println("Dosya okunurken hata oluştu '" + fileName + "'");
        }
    }

    public static void loadBookmark(String URL,TabPane tabPane) {
        com.ankara.tarayici.Browser browser = (com.ankara.tarayici.Browser) tabPane.getTabs().get(tabPane.getSelectionModel().getSelectedIndex()).getContent();
        browser.goURL(URL);
    }

    public static void historyListOpenAndClose(TabPane tabPane) {
        Tab tab = tabPane.getTabs().get(tabPane.getSelectionModel().getSelectedIndex());
        com.ankara.tarayici.Browser browser = (com.ankara.tarayici.Browser) tab.getContent();
        browser.animationListDoor();
    }


}
